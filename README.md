# sat-polarplot.js

SVG Polar Plots for Satellite transits, using [satellite.js](https://github.com/shashwatak/satellite-js) for client-side satellite propagation.

[Example](https://ogn.peanutpod.de/satnogs/sat-polarplot.js/examples/example_svg.html)

## Usage

HTML
```
<svg id="polar1"
    xmlns="http://www.w3.org/2000/svg" version="1.1"
    width="300px"
    height="300px">
</svg>
```

JS
```
var data = {
     tle1: "1 40021U 14033M   18180.54673310  .00000306  00000-0  37368-4 0  9995",
     tle2: "2 40021  97.9160  99.2884 0013952 132.8512 227.3880 14.90326540218794",
     timeframe: {start: "2018-06-30T11:34:50Z",
                 end: "2018-06-30T11:47:29Z"},
     groundstation: {lat: 0.0,
                     lon: 0.0,
                     alt: 10}
};

// Draw orbit in polar plot
var polar1 = new PolarPlotSVG('svg#polar1',
      data.timeframe,
      data.groundstation,
      data.tle1,
      data.tle2);
```

During a pass, the live satellite position can be updated by

```
polar1.update_live();
```

Optionally a timestamp can be provided, e.g. for replay purposes:

```
var t = moment("2018-06-30T11:42:00Z");
polar1.update_live(t);
```

## License

This project is licensed under the GPLv3 license.
