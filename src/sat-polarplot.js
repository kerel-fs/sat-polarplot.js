/* global satellite moment */
/* exported calcPolarPlotSVG */
'use strict';

const pi = Math.PI;
const deg2rad = pi / 180.0;
const rad2deg = 180 / pi;
const svg_namespace = 'http://www.w3.org/2000/svg';


/**
 * @typedef {Object} timeframe
 * @property {Date} start - Start of the observation.
 * @property {Date} end - End of the observation.
 */

class PolarPlotSVG {
    /**
     * Returns a polar plot of a pass at the given groundstation as SVG in a string.
     *
     * @param {timeframe} Timeframe of the oberservation.
     * @param {groundstation} The observing groundstation.
     * @param {tleLine1} TLE line 1 of the observed satellite.
     * @param {tleLine2} TLE line 2 of the observed satellite.
     */
    constructor(container, timeframe, groundstation, tleLine1, tleLine2) {
        this.container = container;
        this.timeframe = {start: moment(timeframe.start),
                          end: moment(timeframe.end)};
        this.groundstation = groundstation;
        this.tleLine1 = tleLine1;
        this.tleLine2 = tleLine2;

        // Get the observer at lat/lon in RADIANS, altitude in km above ground (NOTE: BUG, should be elevation?)
        var observerGd = {
            longitude: groundstation.lon * deg2rad,
            latitude: groundstation.lat * deg2rad,
            height: groundstation.alt / 1000
        };
        this.observerGd = observerGd;

        // Initialize the satellite record
        this.satrec = satellite.twoline2satrec(tleLine1, tleLine2);

        this.draw_axis();

        // Draw the orbit pass on the polar az/el plot
        this.draw_track();

        // Draw observation start and end
        this.draw_position(this.timeframe.start, 'lightgreen');
        this.draw_position(this.timeframe.end, 'red');
    }

    polarGetAzEl(t) {
        var positionAndVelocity = satellite.propagate(this.satrec, t.toDate());

        var gmst = satellite.gstime(t.toDate());
        var positionEci   = positionAndVelocity.position;
        var positionEcf   = satellite.eciToEcf(positionEci, gmst);

        var lookAngles    = satellite.ecfToLookAngles(this.observerGd, positionEcf);

        return {'azimuth': lookAngles.azimuth * rad2deg,
                'elevation': lookAngles.elevation * rad2deg};
    }

    polarGetXY(t) {
        var sky_position = this.polarGetAzEl(t);

        var ret = new Object();
        ret.x = (90 - sky_position.elevation) * Math.sin(sky_position.azimuth * deg2rad);
        ret.y = (sky_position.elevation - 90) * Math.cos(sky_position.azimuth * deg2rad);
        return ret;
    };

    draw_axis() {
        //  <circle fill="none" stroke="black" cx="0" cy="0" r="30"></circle>
        var elements = [];
        for (var i = 1; i <= 3; i++) {
            var circle = document.createElementNS(svg_namespace, 'circle');
            circle.setAttributeNS(null, 'fill', 'none');
            circle.setAttributeNS(null, 'stroke', 'black');
            circle.setAttributeNS(null, 'cx', 0);
            circle.setAttributeNS(null, 'cy', 0);
            circle.setAttributeNS(null, 'r', i*30);
            elements.push(circle);
        }

        //  <path fill="none" stroke="black" stroke-width="1" d="M 0 -95 v 190 M -95 0 h 190"></path>
        var cross = document.createElementNS(svg_namespace, 'path');
        cross.setAttributeNS(null, 'fill', 'none');
        cross.setAttributeNS(null, 'stroke', 'black');
        cross.setAttributeNS(null, 'stroke-width', 1);
        cross.setAttributeNS(null, 'd', 'M 0 -95 v 190 M -95 0 h 190');
        elements.push(cross);

        //  <text x="-4" y="-96">
        //      N
        //  </text>
        var xs = [-4, -4, 96, -106];
        var ys = [-96, 105, 4, 4];
        var ls = ['N', 'S', 'E', 'W'];
        var labels = [];
        for (var i = 0; i <= 3; i++) {
            var label = document.createElementNS(svg_namespace, 'text');
            label.setAttributeNS(null, 'x', xs[i]);
            label.setAttributeNS(null, 'y', ys[i]);
            var l = document.createTextNode(ls[i]);
            label.appendChild(l);
            elements.push(label);
        }

        // viewBox="-110 -110 220 220"
        $(this.container).attr('viewBox', '-110 -110 220 220');
        $(this.container).append(elements);
    }

    draw_position(t, color_fill, id) {
        var point = document.createElementNS(svg_namespace, 'circle');
        point.setAttributeNS(null, 'fill', color_fill);
        point.setAttributeNS(null, 'stroke', 'black');
        point.setAttributeNS(null, 'stroke-width', '1');

        var coord = this.polarGetXY(t);

        point.setAttribute('cx', coord.x);
        point.setAttribute('cy', coord.y);
        point.setAttribute('r', 7);

        if (!(typeof id === 'undefined')) {
            point.setAttributeNS(null, 'id', id);
        }

        $(this.container).append(point);
    }

    draw_track() {
        var polarOrbit = document.createElementNS(svg_namespace, 'path');
        polarOrbit.setAttributeNS(null, 'id', 'track');
        polarOrbit.setAttributeNS(null, 'fill', 'none');
        polarOrbit.setAttributeNS(null, 'stroke', 'blue');
        polarOrbit.setAttributeNS(null, 'stroke-opacity', '1.0');
        polarOrbit.setAttributeNS(null, 'stroke-width', '3');

        var g = '';
        for (var t = moment(this.timeframe.start); t < this.timeframe.end; t.add(20, 's')) {
            var coord = this.polarGetXY(t);

            if (g == '') {
                // Start of line
                g += 'M';
            } else {
                // Continue line
                g += ' L';
            }
            g += coord.x + ' ' + coord.y;
        }
        polarOrbit.setAttribute('d', g);

        $(this.container).append(polarOrbit);
    }

    update_live(t) {
        if (typeof t === 'undefined') {
            var t = moment();
        }
        var point = $(this.container).find('#current');

        if (t.isAfter(this.timeframe.start)
            && t.isBefore(this.timeframe.end)) {

            if (!point.length) {
                // Add live position
                this.draw_position(t, 'blue', 'current');
            } else {
                // Update live position
                var coord = this.polarGetXY(t);

                point.attr('cx', coord.x);
                point.attr('cy', coord.y);
            }
        } else {
            // Remove live position
            point.remove();
        }
    }
}
